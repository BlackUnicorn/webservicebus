﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Messages;
using NServiceBus;
using WebServiceBus.Models;

namespace WebServiceBus.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEndpointInstance endpointInstance;

        public HomeController(IEndpointInstance endpointInstance)
        {
            this.endpointInstance = endpointInstance;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GenerateDocument(string content)
        {
            var id = Guid.NewGuid();
            await this.endpointInstance.Send(new CreateDocument { DocumentId = id, Content = content });
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllDocuments()
        {
            var context = new DocumentsServiceBusEntities();
            return Json(context.Documents.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDocument(Guid? id)
        {
            var context = new DocumentsServiceBusEntities();
            return Json(context.Documents.FirstOrDefault(x => x.Id == id), JsonRequestBehavior.AllowGet);
        }
    }
}