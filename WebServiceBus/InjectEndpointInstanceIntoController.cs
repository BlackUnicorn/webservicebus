﻿using System;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using NServiceBus;

namespace WebServiceBus
{
    public class InjectEndpointInstanceIntoController : DefaultControllerFactory
    {
        private IEndpointInstance _endpointInstance;

        public InjectEndpointInstanceIntoController(IEndpointInstance endpointInstance)
        {
            _endpointInstance = endpointInstance;
        }

        public override IController CreateController(RequestContext requestContext, string controllerName)
        {
            var controllerType = GetControllerType(requestContext, controllerName);
            return (IController)Activator.CreateInstance(controllerType, BindingFlags.CreateInstance, null, new object[]
            {
                _endpointInstance
            }, null);
        }
    }
}