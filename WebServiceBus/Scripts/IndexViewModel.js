﻿function IndexViewModel() {
    let self = this;
    self.DocumentBody = ko.observable("");
    self.Documents = ko.observableArray();
    self.WaitingDocuments = [];
    self.GenerateDocument = function () {
        $.ajax({
            url: '/Home/GenerateDocument/' + self.DocumentBody() ,
            cache: false,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            data: null,
            success: function (id) {
                self.DocumentBody("");
                let numb = setInterval(function () {
                    $.ajax({
                        url: '/Home/GetDocument?id=' + id,
                        cache: false,
                        type: 'GET',
                        contentType: 'application/json; charset=utf-8',
                        data: null,
                        success: function (data) {
                            if (data !== null) {
                                self.Documents.push({ id: data.Id, content: data.Content });
                                clearInterval(self.WaitingDocuments.find(function (value) { return value.id === id; }).number);
                            }
                        }
                    });
                }, 1000);
                self.WaitingDocuments.push({ id: id, number: numb });
            }
        });
    };

    $.ajax({
        url: '/Home/GetAllDocuments',
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: null,
        success: function (data) {
            for (doc of data) {
                self.Documents.push({ id: doc.Id, content: doc.Content });
            }
        }
    });
}
ko.applyBindings(new IndexViewModel());