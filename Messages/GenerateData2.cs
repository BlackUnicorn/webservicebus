﻿using System;
using NServiceBus;

namespace Messages
{
    public class GenerateData2 : ICommand
    {
        public Guid DocumentId { get; set; }
    }
}