﻿using System;
using NServiceBus;

namespace Messages
{
    public class Data2Generated : IEvent
    {
        public Guid DocumentId { get; set; }
        public string Message { get; set; }
    }
}