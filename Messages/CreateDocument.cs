﻿using System;
using NServiceBus;

namespace Messages
{
    public class CreateDocument : ICommand
    {
        public Guid DocumentId { get; set; }
        public string Content { get; set; }
    }
}