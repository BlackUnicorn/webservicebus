﻿using System;
using NServiceBus;

namespace Messages
{
    public class GenerateData1 : ICommand
    {
        public Guid DocumentId { get; set; }
    }
}