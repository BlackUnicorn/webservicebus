﻿using System;
using System.Threading.Tasks;
using NServiceBus;

namespace Service1
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            Console.Title = "Service1";
            var endpointConfiguration = new EndpointConfiguration("Service1");

            endpointConfiguration.SendFailedMessagesTo("error");
            endpointConfiguration.AuditProcessedMessagesTo("audit");

            var transport = endpointConfiguration.UseTransport<LearningTransport>();

            var endpointInstance = await Endpoint.Start(endpointConfiguration)
                .ConfigureAwait(false);
            Console.WriteLine("Press any key to end.");
            Console.ReadKey();
            await endpointInstance.Stop();
        }
    }
}