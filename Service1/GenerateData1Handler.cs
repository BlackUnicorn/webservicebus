﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Messages;
using NServiceBus;
using NServiceBus.Logging;

namespace Service1
{
    public class GenerateData1Handler : 
        IHandleMessages<GenerateData1>
    {
        private static ILog _logger = LogManager.GetLogger<GenerateData1Handler>();
        private static Random _random = new Random();

        public Task Handle(GenerateData1 message, IMessageHandlerContext context)
        {
            _logger.Info($"Recieved message with document Id {message.DocumentId}.");
            _logger.Info($"Generating data1");
            var time = _random.Next(0, 10);
            Thread.Sleep(TimeSpan.FromSeconds(time));
            _logger.Info($"Generated data1 after {time}s");
            return context.Publish(new Data1Generated { DocumentId = message.DocumentId, Message = $"It took {time}s to generate data1" });
        }
    }
}