﻿using System;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Messages;
using NServiceBus;
using NServiceBus.Logging;

namespace PrintService
{
    public class CreateDocumentPolicy : 
        Saga<CreateDocumentData>, 
        IAmStartedByMessages<CreateDocument>, 
        IHandleMessages<Data1Generated>, 
        IHandleMessages<Data2Generated>, 
        IHandleTimeouts<CreateDocumentTimeOut>
    {
        private static ILog _logger = LogManager.GetLogger<CreateDocumentPolicy>();
        
        public async Task Handle(CreateDocument message, IMessageHandlerContext context)
        {
            _logger.Info($"Recieved message with document Id {message.DocumentId} and content \"{message.Content}\".");
            Data.DocumentContent = message.Content;
            await context.Send(new GenerateData1 { DocumentId = message.DocumentId });
            await context.Send(new GenerateData2 { DocumentId = message.DocumentId });
            await RequestTimeout<CreateDocumentTimeOut>(context, TimeSpan.FromSeconds(15));

        }

        public Task Handle(Data1Generated message, IMessageHandlerContext context)
        {
            _logger.Info($"Recieved data1 with document Id {message.DocumentId} and content \"{message.Message}\".");
            Data.Data1 = message.Message;
            return GenerateFile(false);
        }

        public Task Handle(Data2Generated message, IMessageHandlerContext context)
        {
            _logger.Info($"Recieved data2 with document Id {message.DocumentId} and content \"{message.Message}\".");
            Data.Data2 = message.Message;
            return GenerateFile(false);
        }

        public Task Timeout(CreateDocumentTimeOut state, IMessageHandlerContext context)
        {
            _logger.Info($"Entering TimeOut with document Id {Data.DocumentId}.");
            return GenerateFile(true);
        }

        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<CreateDocumentData> mapper)
        {
            mapper.ConfigureMapping<CreateDocument>(x => x.DocumentId).ToSaga(x => x.DocumentId);
            mapper.ConfigureMapping<Data1Generated>(x => x.DocumentId).ToSaga(x => x.DocumentId);
            mapper.ConfigureMapping<Data2Generated>(x => x.DocumentId).ToSaga(x => x.DocumentId);
        }

        private Task GenerateFile(bool fromTimeOut)
        {
            _logger.Info($"Processing data, document Id ={Data.DocumentId}, Data1: {Data.Data1}, Data2: {Data.Data2}, fromTimeOut {fromTimeOut}.");
            if((string.IsNullOrEmpty(Data.Data1) == false && string.IsNullOrEmpty(Data.Data2) == false) || fromTimeOut)
            {
                _logger.Info($"Generating document...");
                var context = new DocumentsServiceBusEntities();
                var content = new StringBuilder();
                content.AppendLine($"Content : {Data.DocumentContent}");
                if(string.IsNullOrEmpty(Data.Data1) == false)
                {
                    content.AppendLine($"Data1 : {Data.Data1}");
                }
                if (string.IsNullOrEmpty(Data.Data2) == false)
                {
                    content.AppendLine($"Data2 : {Data.Data2}");
                }
                if (fromTimeOut)
                {
                    content.AppendLine($"TimeOut reached");
                }
                context.Documents.Add(new Document { Id = Data.DocumentId, Content = content.ToString() });
                context.SaveChanges();
                MarkAsComplete();
            }
            return Task.CompletedTask;
        }
    }
}