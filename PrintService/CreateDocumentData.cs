﻿using System;
using NServiceBus;

namespace PrintService
{
    public class CreateDocumentData : ContainSagaData
    {
        public Guid DocumentId { get; set; }
        public string DocumentContent { get; set; }
        public string Data1 { get; set; }
        public string Data2 { get; set; }
    }
}