﻿using System;
using System.Threading.Tasks;
using Messages;
using NServiceBus;

namespace PrintService
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            Console.Title = "PrintService";
            var endpointConfiguration = new EndpointConfiguration("PrintService");

            endpointConfiguration.SendFailedMessagesTo("error");
            endpointConfiguration.AuditProcessedMessagesTo("audit");

            var transport = endpointConfiguration.UseTransport<LearningTransport>();
            var persistance = endpointConfiguration.UsePersistence<LearningPersistence>();

            var routing = transport.Routing();
            routing.RouteToEndpoint(typeof(GenerateData1), "Service1");
            routing.RouteToEndpoint(typeof(GenerateData2), "Service2");

            var endpointInstance = await Endpoint.Start(endpointConfiguration)
                .ConfigureAwait(false);
            Console.WriteLine("Press any key to end.");
            Console.ReadKey();
            await endpointInstance.Stop();
        }
    }
}