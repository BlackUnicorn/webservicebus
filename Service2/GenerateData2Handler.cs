﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Messages;
using NServiceBus;
using NServiceBus.Logging;

namespace Service2
{
    public class GenerateData2Handler :
        IHandleMessages<GenerateData2>
    {
        private static ILog _logger = LogManager.GetLogger<GenerateData2Handler>();
        private static Random _random = new Random();

        public Task Handle(GenerateData2 message, IMessageHandlerContext context)
        {
            _logger.Info($"Recieved message with document Id {message.DocumentId}.");
            _logger.Info($"Generating data2");
            var time = _random.Next(0, 10);
            Thread.Sleep(TimeSpan.FromSeconds(time));
            _logger.Info($"Generated data2 after {time}s");
            return context.Publish(new Data2Generated { DocumentId = message.DocumentId, Message = $"It took {time}s to generate data2" });
        }
    }
}