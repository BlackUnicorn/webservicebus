﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NServiceBus;

namespace Service2
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.Title = "Service2";
            var endpointConfiguration = new EndpointConfiguration("Service2");

            endpointConfiguration.SendFailedMessagesTo("error");
            endpointConfiguration.AuditProcessedMessagesTo("audit");

            var transport = endpointConfiguration.UseTransport<LearningTransport>();

            var endpointInstance = await Endpoint.Start(endpointConfiguration)
                .ConfigureAwait(false);
            Console.WriteLine("Press any key to end.");
            Console.ReadKey();
            await endpointInstance.Stop();
        }
    }
}
